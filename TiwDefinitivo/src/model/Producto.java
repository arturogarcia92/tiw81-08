package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Table;

/**
 * The persistent class for the productos database table.
 * 
 */
@Entity
@Table(name = "productos")
@NamedQuery(name = "Producto.findAll", query = "SELECT p FROM Producto p")
public class Producto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private int productoid;

    private String descripcion;

    private String email;

    @Lob
    private String imagen;

    @Column(name = "informacion_estado")
    private String informacionEstado;

    private String nombre;

    private String nombrecategoria;

    private float precio;

    private int puntos;

    private int visitas;

    public Producto() {
    }

    public int getProductoid() {
        return this.productoid;
    }

    public void setProductoid(int productoid) {
        this.productoid = productoid;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImagen() {
        return this.imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getInformacionEstado() {
        return this.informacionEstado;
    }

    public void setInformacionEstado(String informacionEstado) {
        this.informacionEstado = informacionEstado;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombrecategoria() {
        return this.nombrecategoria;
    }

    public void setNombrecategoria(String nombrecategoria) {
        this.nombrecategoria = nombrecategoria;
    }

    public float getPrecio() {
        return this.precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getPuntos() {
        return this.puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getVisitas() {
        return this.visitas;
    }

    public void setVisitas(int visitas) {
        this.visitas = visitas;
    }

    /*metodo que comprueba si el producto que recibe ya existe*/
    public static boolean prodDuplicado(Producto p1) {

        p1.getDescripcion();
        p1.getEmail();

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        Query query = entitymanager
                .createQuery("SELECT u FROM Producto u WHERE u.email = :email AND u.descripcion = :descripcion");

        List<Producto> list = query.setParameter("email", p1.getEmail())
                .setParameter("descripcion", p1.getDescripcion()).getResultList();

        //Si el producto ya existe 
        if (list.size() != 0) {
            return false;
        } else {    //Si el producto NO existe 
            return true;
        }

    }
    
    /*metodo que inserta el producto que recibe */
    public static boolean insertarProducto(Producto p1) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager em = emfactory.createEntityManager();

        //Si el producto No esta duplicado lo insertamos
        if (p1.prodDuplicado(p1)) {

            em.getTransaction().begin();
            em.persist(p1);
            em.getTransaction().commit();

            return true;
        }
        //Si el producto esta duplicado NO lo insertamos
        else
            return false;
    }
    
    /*metodo que devuelve todos los productos de la base de datos */
    public List<Producto> devolverProductos() {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        Query query = entitymanager.createQuery("SELECT p FROM Producto p");

        List<Producto> list = query.getResultList();
        
        if (!list.isEmpty()) {

            return list;
        }

        return null;

    }
    /*metodo que devuelve los productos de un usuario en concreto */
    public List<Producto> devolverProductoUsuario(String email) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        Query query = entitymanager.createQuery("SELECT p FROM Producto p WHERE p.email = :email");

        List<Producto> list = query.setParameter("email", email).getResultList();

        if (!list.isEmpty()) {

            return list;

        }

        return null;
    }
    
    /*metodo que borra todos los productos asociados a un usuario en concreto */
    public void borrarProductosUsuario(String email){
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        EntityTransaction etx = entitymanager.getTransaction();
        etx.begin();
        
        Query query = entitymanager.createQuery("DELETE FROM Producto r WHERE r.email = :email");
        
        query.setParameter("email", email).executeUpdate();
        
        etx.commit();
        
        
    }
    /*metodo que devuelve un productos en concreto */
    public Producto devolverProducto(int id) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        Query query = entitymanager.createQuery("SELECT p FROM Producto p WHERE p.productoid = :productoid");

        List<Producto> list = query.setParameter("productoid", id).getResultList();

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }
    /*metodo que modifica los campos de un producto en concreto */
    public void modificarProduct(Producto p1) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        Producto p2 = new Producto();

        p2 = devolverProducto(p1.productoid);

        entitymanager.getTransaction().begin();

        Query queryName = entitymanager
                .createQuery("update Producto r set r.nombre = :nuevonombre where r.productoid = :id");
        Query queryDesc = entitymanager
                .createQuery("update Producto r set r.descripcion = :nuevodescripcion where r.productoid = :id");
        Query queryPrecio = entitymanager
                .createQuery("update Producto r set r.precio = :nuevoprecio where r.productoid = :id");
        Query queryCateg = entitymanager
                .createQuery("update Producto r set r.nombrecategoria = :nuevocategoria where r.productoid = :id");
        Query queryEstado = entitymanager
                .createQuery("update Producto r set r.informacionEstado  = :nuevoestado where r.productoid = :id");

        
        /*si algun campo no lo actualizamos, dejar� los datos del anterior, por eso se han creado 2 productos */

        if (p1.getNombre() == null) {
            queryName.setParameter("nuevonombre", p2.getNombre()).setParameter("id", p2.getProductoid())
                    .executeUpdate();
    
        }

        else {
            queryName.setParameter("nuevonombre", p1.getNombre()).setParameter("id", p1.getProductoid())
                    .executeUpdate();
            
        }


        if (p1.getDescripcion() == null) {
            queryDesc.setParameter("nuevodescripcion", p2.getDescripcion()).setParameter("id", p2.getProductoid())
                    .executeUpdate();
        
        }

        else {
            queryDesc.setParameter("nuevodescripcion", p1.getDescripcion()).setParameter("id", p1.getProductoid())
                    .executeUpdate();
        }

        Float precio = p1.getPrecio();

        
        if (precio.equals(0)) {
            queryPrecio.setParameter("nuevoprecio", p2.getPrecio()).setParameter("id", p2.getProductoid())
                    .executeUpdate();
        }

        else {
            queryPrecio.setParameter("nuevoprecio", p1.getPrecio()).setParameter("id", p1.getProductoid())
                    .executeUpdate();
        }


        if (p1.getNombrecategoria() == null) {
            queryCateg.setParameter("nuevocategoria", p2.getNombrecategoria()).setParameter("id", p2.getProductoid())
                    .executeUpdate();
        }

        else {
            queryCateg.setParameter("nuevocategoria", p1.getNombrecategoria()).setParameter("id", p1.getProductoid())
                    .executeUpdate();
        }
        
        
        if (p1.getInformacionEstado() == null) {
            queryEstado.setParameter("nuevoestado", p2.getInformacionEstado()).setParameter("id", p2.getProductoid())
                    .executeUpdate();
        }

        else {
            queryEstado.setParameter("nuevoestado", p1.getInformacionEstado()).setParameter("id", p1.getProductoid())
                    .executeUpdate();
        }
        
        entitymanager.getTransaction().commit();

    }

    public static void main(String[] args) {

        Producto p1 = new Producto();

        List<Producto> list = p1.devolverProductos();


    }

    
    
    /*metodo que borra un productonen concreto */   
    public void borrarProducto(Producto p1) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();
    
        EntityTransaction etx = entitymanager.getTransaction();
        etx.begin();
        
        Query query = entitymanager.createQuery("DELETE FROM Producto r WHERE r.productoid = :id");
        
        query.setParameter("id", p1.getProductoid()).executeUpdate();
        

        etx.commit();

    }

}