package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Table;

/**
 * Clase persistente para la tabla usuario de la bbdd
 */
@Entity
@Table(name = "usuario")
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    
    /**
    * Atributos de usuario
    */

    private String apellido1;

    private String apellido2;

    private String calle;

    private String ciudad;

    private String codigopostal;

    private String contraseña;

    private String nombre;

    private String piso;

    private String puerta;

    private String rol;

    private String email;

    /**
    * Construcitor de usuario
    */
    
    public Usuario() {
    }

    /**
    * Getters y setters de usuario
    */
    
    public String getApellido1() {
        return this.apellido1;
    }

    public void setApellido1(String apellido1) {
        this.apellido1 = apellido1;
    }

    public String getApellido2() {
        return this.apellido2;
    }

    public void setApellido2(String apellido2) {
        this.apellido2 = apellido2;
    }

    public String getCalle() {
        return this.calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodigopostal() {
        return this.codigopostal;
    }

    public void setCodigopostal(String codigopostal) {
        this.codigopostal = codigopostal;
    }

    public String getContraseña() {
        return this.contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPiso() {
        return this.piso;
    }

    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return this.puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getRol() {
        return this.rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
    * Metodos de usuario
    */
    
    /**
    * Metodo que comprueba si el nombre de usuario y la contraseña introducida son correctos. Recibe el email del usuario y la contraseña introducida
    */
    public boolean comprobarContraseña(String mail, String passenviada) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();
        Query query = entitymanager.createQuery("SELECT u FROM Usuario u WHERE u.email = :email");
        List<Usuario> list = query.setParameter("email", mail).getResultList();
        String vmail = null;
        String vpass = null;

        for (Usuario u : list) {
            vmail = u.getEmail();
            vpass = u.getContraseña();
        }
        if (mail.equals(vmail)) {
            if (passenviada.equals(vpass)) { //La contraseña almacenada en la bdd coincide con la introducida
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
    * Metodo que comprueba si el usuario introducido ya existe en la bdd. Recibe el email del usuario
    */
    public boolean comprobarUsuario(String email) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();
        Query query = entitymanager.createQuery("SELECT u FROM Usuario u WHERE u.email = :email");
        List<Usuario> list = query.setParameter("email", email).getResultList();
        String mail = null;
        
        if (list.size() != 0) { //Si la query devuelve una lista vacia no existe el usuario
            return false;
        } else {
            return true;
        }
    }
    
    /**
    * Metodo que inserta un usuario en la bbdd. Recibe un objeto Usuario
    */
    public static boolean insertarUsuario(Usuario u1) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager em = emfactory.createEntityManager();
        
        if (u1.comprobarUsuario(u1.getEmail())) { //Comprobar que el nombre de usuario no existe en la bbdd
            em.getTransaction().begin();
            em.persist(u1);
            em.getTransaction().commit();
            return true;
        }
        else
            return false;
    }


    /**
    * Metodo que devuelve un usuario de la bbdd. Recibe el email del usuario
    */
    public Usuario devolverUsuario(String mail) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        System.out.println("ESTAS EN DEVOLVER USUARIOOOO");
        // Scalar function
        Query query = entitymanager.createQuery("SELECT u FROM Usuario u WHERE u.email = :email");

        List<Usuario> list = query.setParameter("email", mail).getResultList();

        Usuario u1 = null;

        if (!list.isEmpty()) {
            u1 = list.get(0);
        }

        return u1;
        
    }
    
    /**
    * Metodo que devuelve todos los usuarios almacenados en la bbdd. No recibe ningun parametro
    */
    public List<Usuario> devolverUsuarios() {
    EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
    EntityManager entitymanager = emfactory.createEntityManager();
    Query query = entitymanager.createQuery("SELECT u FROM Usuario u");
    List<Usuario> list = query.getResultList();
    
    if (!list.isEmpty()) { //Si la lita no esta vacia la devuelve
        return list;
    }
    
    return null;
    }

    /**
    * Metodo que modifica la informacion de un usuario. Recibe un objeto usuario
    */
    public void modificarUsuario(Usuario u1) {
        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();

        Usuario u2 = new Usuario();     
        u2 = devolverUsuario(u1.getEmail());        
        entitymanager.getTransaction().begin();

        Query queryName = entitymanager.createQuery("update Usuario r set r.nombre = :nuevonombre where r.email = :email");
        Query queryApp1 = entitymanager.createQuery("update Usuario r set r.apellido1 = :nuevoapellido1 where r.email = :email");
        Query queryApp2 = entitymanager.createQuery("update Usuario r set r.apellido2 = :nuevoapellido2 where r.email = :email");
        Query queryCalle = entitymanager.createQuery("update Usuario r set r.calle = :nuevocalle where r.email = :email");
        Query queryCiudad= entitymanager.createQuery("update Usuario r set r.ciudad = :nuevociudad where r.email = :email");
        Query queryCodigo= entitymanager.createQuery("update Usuario r set r.codigopostal = :nuevocodigopostal where r.email = :email");
        Query queryPiso= entitymanager.createQuery("update Usuario r set r.piso = :nuevopiso where r.email = :email");
        Query queryPuerta= entitymanager.createQuery("update Usuario r set r.puerta = :nuevopuerta where r.email = :email");
        Query queryRol= entitymanager.createQuery("update Usuario r set r.rol = :nuevorol where r.email = :email");
        
        // Si no modificamos algun campo del usuario dejamos los datos del anterior (usuario2), que es una copia del primero antes de modificarlo
        
        
        if (u1.getNombre() == null) {
            queryName.setParameter("nuevonombre", u2.getNombre()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryName.setParameter("nuevonombre", u1.getNombre()).setParameter("email", u1.getEmail()).executeUpdate();
        }

        if (u1.getApellido1() == null) {
            queryApp1.setParameter("nuevoapellido1", u2.getApellido1()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryApp1.setParameter("nuevoapellido1", u1.getApellido1()).setParameter("email", u1.getEmail()).executeUpdate();
        }

        if (u1.getApellido2() == null) {
            queryApp2.setParameter("nuevoapellido2", u2.getApellido2()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryApp2.setParameter("nuevoapellido2", u1.getApellido2()).setParameter("email", u1.getEmail()).executeUpdate();
        }

        if (u1.getCalle() == null) {
            queryCalle.setParameter("nuevocalle", u2.getCalle()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryCalle.setParameter("nuevocalle", u1.getCalle()).setParameter("email", u1.getEmail()).executeUpdate();
        }

        if (u1.getCiudad() == null) {
            queryCiudad.setParameter("nuevociudad", u2.getCiudad()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryCiudad.setParameter("nuevociudad", u1.getCiudad()).setParameter("email", u1.getEmail()).executeUpdate();
        }
        
        if (u1.getCodigopostal() == null) {
            queryCodigo.setParameter("nuevocodigopostal", u2.getCodigopostal()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryCodigo.setParameter("nuevocodigopostal", u1.getCodigopostal()).setParameter("email", u1.getEmail()).executeUpdate();
        }
        
        if (u1.getPiso() == null) {
            queryPiso.setParameter("nuevopiso", u2.getPiso()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryPiso.setParameter("nuevopiso", u1.getPiso()).setParameter("email", u1.getEmail()).executeUpdate();
        }
        
        if (u1.getPuerta() == null) {
            queryPuerta.setParameter("nuevopuerta", u2.getPuerta()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryPuerta.setParameter("nuevopuerta", u1.getPuerta()).setParameter("email", u1.getEmail()).executeUpdate();
        }
        
        if (u1.getRol() == null) {
            queryRol.setParameter("nuevorol", u2.getRol()).setParameter("email", u2.getEmail()).executeUpdate();
        }
        else {
            queryRol.setParameter("nuevorol", u1.getRol()).setParameter("email", u1.getEmail()).executeUpdate();
        }
        
        entitymanager.getTransaction().commit();

    }

    /**
    * Metodo que elimina un usuario de la bbdd. Recibe un objeto de tipo Usuario
    */
    public void borrarUsuario(Usuario u1) {

        EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("TiwDefinitivo");
        EntityManager entitymanager = emfactory.createEntityManager();
        
        EntityTransaction etx = entitymanager.getTransaction();
        etx.begin();        
        Producto p1 = new Producto();
        p1.borrarProductosUsuario(u1.getEmail()); //Tambien se borran todos sus productos
        Query query = entitymanager.createQuery("DELETE FROM Usuario r WHERE r.email = :id");  //Ejecutar la query
        query.setParameter("id", u1.getEmail()).executeUpdate();
        
        etx.commit();
    }
}