
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.glassfish.jersey.media.multipart.MultiPart;

import com.mysql.jdbc.Blob;
import com.sun.xml.bind.v2.runtime.reflect.Lister.Pack;
import com.sun.xml.ws.runtime.dev.Session;

import JMS.InteraccionMQ;
import model.Producto;
import model.Usuario;
import sun.misc.IOUtils;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet(name = "loginServlet", urlPatterns = { "/loginServlet" })
@MultipartConfig
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public loginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		/*
		 * Servlet controlador, seg�n el par�metro realizar� una acci�n u otra
		 */

		if ("login".equals(request.getParameter("logear"))) {
			// Logueo del usuario

			login(request, response);
			// lista productos
		}

		if ("register".equals(request.getParameter("registrar"))) {

			registrar(request, response);
		}

		if ("logout".equals(request.getParameter("param"))) {

			System.out.println("entro al if de cerrar sesi�n");

			logout(request, response);
		}

		if ("insertar".equals(request.getParameter("insertarproducto"))) {

			insertarproducto(request, response);
		}

		if ("mostrarProd".equals(request.getParameter("Mostrar"))) {

			mostrarProducto(request, response);
		}

		if ("mostrarUser".equals(request.getParameter("MostrarUser"))) {

			mostrarUsuario(request, response);
		}

		if ("editarProducto".equals(request.getParameter("editarproducto"))) {

			editarProducto(request, response);
		}

		if ("borrarProducto".equals(request.getParameter("borrarproducto"))) {

			borrarProducto(request, response);
		}

		if ("editarUsuario".equals(request.getParameter("editarusuario"))) {

			editarUsuario(request, response);
		}

		if ("borrarUsuario".equals(request.getParameter("borrarusuario"))) {

			borrarUsuario(request, response);
		}

		if ("enviarMensaje".equals(request.getParameter("enviarmensaje"))) {

			enviarMensaje(request, response);

		}

		if ("leerMensaje".equals(request.getParameter("leermensaje"))) {

			leerMensaje(request, response);

		}

	}

	/*
	 * Borrar� un usuario del sistema junto a sus productos, buscar� el usuario
	 * por id
	 */

	private void borrarUsuario(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		Usuario u1 = new Usuario();

		u1.setEmail(request.getParameter("userid"));

		u1.borrarUsuario(u1);

		List<Usuario> usuarios = u1.devolverUsuarios();

		session.setAttribute("listaUsuarios", usuarios);
		RequestDispatcher view = request.getRequestDispatcher("logedin.jsp");
		view.forward(request, response);
	}

	/*
	 * Editar� el usuario del sistema en funcion de los valores recibidos desde
	 * el formulario
	 */
	private void editarUsuario(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		Usuario u1 = new Usuario();

		
		
		u1.setEmail((String) request.getParameter("emailid"));

		
		
		if (!request.getParameter("nombre").isEmpty()) {
			u1.setNombre(request.getParameter("nombre"));
		}

		if (!request.getParameter("apellido1").isEmpty()) {
			u1.setApellido1(request.getParameter("apellido1"));
		}

		if (!request.getParameter("apellido2").isEmpty()) {
			u1.setApellido2(request.getParameter("apellido2"));
		}

		if (!request.getParameter("calle").isEmpty()) {
			u1.setCalle(request.getParameter("calle"));
		}

		if (!request.getParameter("ciudad").isEmpty()) {
			u1.setCiudad(request.getParameter("ciudad"));
		}

		if (!request.getParameter("codigopostal").isEmpty()) {
			u1.setCodigopostal(request.getParameter("codigopostal"));
		}

		if (!request.getParameter("piso").isEmpty()) {
			u1.setPiso(request.getParameter("piso"));
		}

		if (!request.getParameter("puerta").isEmpty()) {
			u1.setPuerta(request.getParameter("puerta"));
		}

		if (!request.getParameter("rol").isEmpty()) {
			u1.setRol(request.getParameter("rol"));
		}

		// Llamamos al m�todo para modificar el usuario

		u1.modificarUsuario(u1);

		// Creamos la lista de usuarios y la guardamos en la session, para
		// obtener los �ltimos cambios
		List<Usuario> usuarios = u1.devolverUsuarios();

		Usuario u2 = u1.devolverUsuario(u1.getEmail());

		session.setAttribute("usuario", u2);

		session.setAttribute("username", u2.getNombre().toUpperCase());
		session.setAttribute("email", u2.getEmail());
		
		
		session.setAttribute("listaUsuarios", usuarios);

		RequestDispatcher view = request.getRequestDispatcher("logedin.jsp");
		view.forward(request, response);
	}

	/*
	 * M�todo para enviar mensajes por el chat, el mensaje ir� dirigido al id
	 * del creador del anuncio
	 */
	private void enviarMensaje(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();

		String emisor = session.getAttribute("email").toString() + " ";

		InteraccionMQ mq = new InteraccionMQ();

		String id = request.getParameter("id");

		String mensaje = emisor + ":" + request.getParameter("mensaje");

		mq.escrituraMQ(mensaje, id);

		RequestDispatcher view = request.getRequestDispatcher("logedin.jsp");
		view.forward(request, response);

	}

	/* M�todo para leer los mensajes recibidos */
	private void leerMensaje(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		InteraccionMQ mq = new InteraccionMQ();

		String strAux;

		HttpSession session = request.getSession();

		String lector = session.getAttribute("email").toString();

		strAux = mq.lecturaMQ(lector);

		request.setAttribute("mensajes", strAux);

		RequestDispatcher view = request.getRequestDispatcher("logedin.jsp");
		view.forward(request, response);
	}

	/* M�todo para mostrar el usuario desde el panel de administraci�n */
	private void mostrarUsuario(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub

		Usuario u1 = new Usuario();
		String email = request.getParameter("emailid");

		u1 = u1.devolverUsuario(email);

		if (u1 != null) {
			request.setAttribute("usuario", u1);
			try {
				request.getRequestDispatcher("./profileuser.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * M�todo que borrar� el producto recibido por par�metro desde el formulario
	 * del propio producto
	 */
	private void borrarProducto(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();

		Producto p1 = new Producto();
		String id = request.getParameter("id");
		int idProducto = Integer.parseInt(id);

		p1.setProductoid(idProducto);
		p1.borrarProducto(p1);

		// Actualizamos la lista de productos del usuario y la guardamos en la
		// sesi�n para refrescarla.
		List<Producto> productos = p1.devolverProductoUsuario(session.getAttribute("email").toString());

		session.setAttribute("listaProductos", productos);
		request.setAttribute("listaProductos", productos);
		RequestDispatcher view = request.getRequestDispatcher("logedin.jsp");
		view.forward(request, response);

	}

	/*M�todo para editar un producto, en funci�n del formulario. Si un campo no se toca se quedar� como estaba */
	private void editarProducto(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();

		Producto p1 = new Producto();

		// En el caso de que sean nulos los dejamos como estaban.
		p1.setEmail((String) session.getAttribute("email"));
		if (!request.getParameter("categoria").isEmpty()) {
			p1.setNombrecategoria(request.getParameter("categoria"));
		}

		p1.setInformacionEstado("VENTA");

		if (!request.getParameter("precio").isEmpty()) {
			p1.setPrecio(Float.parseFloat(request.getParameter("precio")));
		}

		if (!request.getParameter("nombre").isEmpty()) {
			p1.setNombre(request.getParameter("nombre"));
		}

		if (!request.getParameter("descripcion").isEmpty()) {
			p1.setDescripcion(request.getParameter("descripcion"));
		}

		if (!request.getParameter("estado").isEmpty()) {
			p1.setInformacionEstado(request.getParameter("estado"));
		}

		String id = request.getParameter("id");
		int idProducto = Integer.parseInt(id);

		p1.setProductoid(idProducto);
		p1.modificarProduct(p1);

		//recargamos la lista de productos.
		List<Producto> productos = p1.devolverProductoUsuario(session.getAttribute("email").toString());

		session.setAttribute("listaProductosUsuario", productos);

		mostrarProducto(request, response);

	}
    /*M�todo para mostrar un producto cuando hagamos click en el, recoger� la id y mostrar� la informaci�n*/
	private void mostrarProducto(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub

		Producto p1 = new Producto();
		String id = request.getParameter("id");
		int idProducto = Integer.parseInt(id);

		p1 = p1.devolverProducto(idProducto);

		if (p1 != null) {
			request.setAttribute("producto", p1);
			try {
				request.getRequestDispatcher("./product.jsp").forward(request, response);
			} catch (ServletException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	
	/*M�todo para insertar productos nuevos en funci�n de los datos del formulario */
	@SuppressWarnings("resource")
	private void insertarproducto(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		Producto p1 = new Producto();

		//Ruta de destino donde almacenaremos las im�genes.
		
		String pathDesti = "/home/tiw/git/tiw81-08/TiwDefinitivo/WebContent/images/";
		Part part = request.getPart("imagen");
		String fileName = part.getSubmittedFileName();

		// aplicamos hash para evitar archivos duplicados.
		int hash = fileName.hashCode();

		String nombrenuevo = hash + ".jpg";

		OutputStream outstream = null;
		InputStream inputstream = null;

		outstream = new FileOutputStream(new File(pathDesti + File.separator + nombrenuevo));

		inputstream = part.getInputStream();
		int read = 0;
		byte[] arr = new byte[1024];

		while ((read = inputstream.read(arr)) != -1) {
			outstream.write(arr, 0, read);
		}

		p1.setEmail((String) session.getAttribute("email"));
		p1.setNombrecategoria(request.getParameter("categoria"));
		p1.setInformacionEstado("VENTA");
		p1.setPrecio(Float.parseFloat(request.getParameter("precio")));
		p1.setNombre(request.getParameter("nombre"));
		p1.setDescripcion(request.getParameter("descripcion"));
		p1.setImagen("/TiwDefinitivo/images/" + nombrenuevo);


		// Si entra significa que el producto no est� duplicado
		if (p1.insertarProducto(p1)) {

			List<Producto> productos = p1.devolverProductos();
			session.setAttribute("listaProductos", productos);
			List<Producto> productosUsuario = p1.devolverProductoUsuario(session.getAttribute("email").toString());
			session.setAttribute("listaProductosUsuario", productosUsuario);

			String status = "Producto a�adido correctamente, puedes verlo o editarlo en tu perfil";
			request.setAttribute("insertProductStatus", status);
			RequestDispatcher view = request.getRequestDispatcher("addproduct.jsp");
			view.forward(request, response);

		}

		else {

		}

	}

	/*M�todo para cerrar la sesi�n del sistema, la invalida.*/
	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		response.setContentType("text/html");

		HttpSession session = request.getSession();
		session.invalidate();

		String status = "�Te has desconectado, vuelve pronto!";

		request.setAttribute("logoutStatus", status);

		RequestDispatcher view = request.getRequestDispatcher("home.jsp");
		view.forward(request, response);

	}

	/*M�todo para logearse en el sistema*/
	void login(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Creamos un nuevo usuario e invalidamos la sesi�n antigua.
		Usuario u1 = new Usuario();
		response.setContentType("text/html");

		HttpSession session1 = request.getSession();
		session1.invalidate();

		HttpSession session = request.getSession();
		String user = request.getParameter("username");
		String pass = request.getParameter("password");

		Usuario u2 = u1.devolverUsuario(user);

		String status = "";

		
		// Comprobamos que el usuario y la contrase�a coinciden y guardamos los datos del usuario en la sesi�n
		if (u1.comprobarContrase�a(user, pass)) {

			Producto p1 = new Producto();

			session.setAttribute("usuario", u2);
			session.setAttribute("username", u2.getNombre().toUpperCase());
			session.setAttribute("email", u2.getEmail());
			session.setAttribute("rol", u2.getRol());
			
			// Guardamos la lista de productos y la de productos del usuario en la sesi�n
			List<Producto> productos = p1.devolverProductos();
			session.setAttribute("listaProductos", productos);
			List<Producto> productosUsuario = p1.devolverProductoUsuario(u2.getEmail());
			session.setAttribute("listaProductosUsuario", productosUsuario);
			
			//Guardamos la lista de usuarios en la sesi�n.
			List<Usuario> usuarios = u1.devolverUsuarios();
			session.setAttribute("listaUsuarios", usuarios);
			
			
			String encodedURL = response.encodeRedirectURL("logedin.jsp");
			System.out.println(encodedURL);
			RequestDispatcher view = request.getRequestDispatcher(encodedURL);
			view.forward(request, response);
		}

		else {

			status = "OOOPS, ha ocurrido un error...El usuario no existe o la contrase�a es incorrecta. Si quieres, �puedes registrarte!";
			request.setAttribute("loginStatus", status);
			RequestDispatcher view = request.getRequestDispatcher("home.jsp");
			view.forward(request, response);
		}

	}

	
	/*M�todo para registrarnos en el sistema en funci�n de los datos del formulario de registro*/
	void registrar(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Usuario u1 = new Usuario();

		
		//Rellenamos los datos del usuario y los insertamos al usuario.
		u1.setNombre(request.getParameter("nombre"));
		u1.setContrase�a(request.getParameter("contrase�a"));
		u1.setApellido1(request.getParameter("primerapellido"));
		u1.setApellido2(request.getParameter("segundoapellido"));
		u1.setCiudad(request.getParameter("ciudad"));
		u1.setCodigopostal(request.getParameter("codpostal"));
		u1.setEmail(request.getParameter("email"));
		u1.setRol("USUARIO");

		//Si no existe el user lo creamos.
		
		if (u1.insertarUsuario(u1)) {
			String status = "Te has registrado correctamente, �Hola " + u1.getNombre()
					+ " Ahora puedes iniciar sesi�n!";
			request.setAttribute("registerStatus", status);
			RequestDispatcher view = request.getRequestDispatcher("home.jsp");
			view.forward(request, response);
		}

		else {
			String status = "El usuario ya existe";
			request.setAttribute("registerStatus", status);
			RequestDispatcher view = request.getRequestDispatcher("home.jsp");
			view.forward(request, response);
		}

	}

}
