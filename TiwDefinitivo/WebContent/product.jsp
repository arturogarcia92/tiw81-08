<%@ page import="java.util.List"%>
<%@ page import="model.Producto"%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Product</title>

<link rel="stylesheet" href="css/styles.css" />
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<%
	if (session.getAttribute("username") == null) {

		response.sendRedirect("home.jsp");

	}
%>


<body id="cuerpo">




	<nav class="navbar navbar-inverse" id="nav">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="logedin.jsp">WALLATIW</a>
		</div>
		<ul class="nav navbar-nav" id="lista">
			<li class="active"><a href="logedin.jsp">INICIO</a></li>
			<li><a href="profile.jsp">TUS PRODUCTOS</a></li>
			<li><a href="addproduct.jsp">A�ADIR PRODUCTOS</a></li>
			<li><a href="yourpersonalprofile.jsp">TU PERFIL</a></li>
			<%
			
			if(session.getAttribute("rol").equals("ADMIN")){
			out.print("<li><a href='userlist.jsp'>ADMIN</a></li>");
			}
			
			%>
			
			<li class="loginnavbar">
				<%
					if (session.getAttribute("username") != "null") {
						out.print("<a href='loginServlet?param=logout'> Hola " + session.getAttribute("username")
								+ "(Cerrar sesi�n) </a> ");

					}
				%>

			</li>
		</ul>
	</div>
	</nav>



	<div id="cleaner"></div>
	<div id="center">



		<div class="productos">



			<div class="row" id="rowprodview">
				<%
					Producto prod = new Producto();
					prod = (Producto) (request.getAttribute("producto"));

					if (prod != null) {
						out.print("<div class='col-md-12 col-xs-12 col-sm-12 col-lg-12' id='productver'>");
						out.print("<div class='tituProduct'>" + prod.getNombre().toString() + "</div>");
						out.print("<div><img id='imgSubida' src="+prod.getImagen()+"></div>");
						out.print("<div class='descProduct'>"  + prod.getDescripcion().toString() + "</div>");
						out.print("<p class='precioProduct'> Precio : " + prod.getPrecio() + " Euros </p>");
						out.print("<p class='categoriaProduct'>" + prod.getNombrecategoria().toString() + "</p>");
						out.print("<p class='emailProduct'> " + prod.getEmail().toString() + "</p>");
						out.print("</div>");
					}

				%>
			</div>
		</div>
		
		<div id="cleaner">
		
		<%
		
		
		
		if(session.getAttribute("rol").equals("ADMIN")|session.getAttribute("email").equals(prod.getEmail().toString())){
			
			
			out.print("<div id='productadm'>");
			out.print("<form id='formproduct2' action='loginServlet' method='post' enctype='multipart/form-data'>");
			
			out.print("<div class='insertproduct'>");
			out.print("<h1 id='prodedit'>EDICION DEL PRODUCTO</h1>");
			out.print("<input type='text' id='textadmin' placeholder='Nombre del producto' name='nombre'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin'placeholder='Descripción' name='descripcion'>");
			out.print("<br>");		
			out.print("<input type='text' id='textadmin' placeholder='Categoría' name='categoria'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin' placeholder='precio' name='precio'>");
			out.print("<br>");	
			out.print("<select id='estado' name='estado'>");
			out.print("<option value='VENTA'>En venta</option>");
			out.print("<option value='RESERVADO'>Reservado</option>");
			out.print("<option value='VENDIDO'>Vendido</option>");	
			out.print("</select>");
			out.print("<br>");	
			out.print("<div id='editDiv'>");
			out.print("<input id='registproductbutton' type='submit' name='submit' value='Editar'>");
			out.print("</div>");
			out.print("<input type='hidden' name='id' value="+prod.getProductoid()+">");
			out.print("<input type='hidden' value='editarProducto' name='editarproducto'>");
			out.print("</div>");
			out.print("</form>");
			out.print("<form id='formproduct2' action='loginServlet' method='post' enctype='multipart/form-data'>");
			out.print("<input id='registproductbutton' type='submit' name='submit' value='Borrar'>");
			out.print("<input type='hidden' name='id' value="+prod.getProductoid()+">");
			out.print("<input type='hidden' value='borrarProducto' name='borrarproducto'>");
			out.print("</form>");
			out.print("</div>");
			
			
			
		}
		
		
		%>

	<% 
			out.print("<div id='productadm'>");
			out.print("<form id='formproduct2' action='loginServlet' method='post' enctype='multipart/form-data'>");
			out.print("<input type='text' name='mensaje' size='94'>");
			out.print("<input id='registproductbutton' type='submit' name='submit' value='Enviar mensaje'>");		
			out.print("<input type='hidden' name='id' value="+prod.getEmail()+">");
			out.print("<input type='hidden' value='enviarMensaje' name='enviarmensaje'>");
			out.print("</form>");
			out.print("</div>");
			
	%>		
				



	</div>

	</div>

</body>
</html>