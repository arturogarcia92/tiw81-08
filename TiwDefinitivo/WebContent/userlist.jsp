
<%@ page import="java.util.List"%>
<%@ page import="model.Producto"%>
<%@ page import="model.Usuario"%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>WallaTiw</title>

<link rel="stylesheet" href="css/styles.css" />
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<%
	if (session.getAttribute("username") == null) {

		response.sendRedirect("home.jsp");

	}
%>


<body id="cuerpo">


	<nav class="navbar navbar-inverse" id="nav">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="logedin.jsp">WALLATIW</a>
		</div>
		<ul class="nav navbar-nav" id="lista">
			<li class="active"><a href="logedin.jsp">INICIO</a></li>
			<li><a href="profile.jsp">TUS PRODUCTOS</a></li>
			<li><a href="addproduct.jsp">A�ADIR PRODUCTOS</a></li>
			<li><a href="yourpersonalprofile.jsp">TU PERFIL</a></li>
			<%
			
			if(session.getAttribute("rol").equals("ADMIN")){
			out.print("<li><a href='userlist.jsp'>ADMIN</a></li>");
			}
			
			%>
			
			<li class="loginnavbar">
				<%
					if (session.getAttribute("username") != "null") {
						out.print("<a href='loginServlet?param=logout'> Hola " + session.getAttribute("username")
								+ "(Cerrar sesi�n) </a> ");

					}
				%>

			</li>
		</ul>
	</div>
	</nav>

	<div id="cleaner"></div>
	<div id="center">


					<%
					if (session.getAttribute("username") != "null") {
						out.print("<div class='holaprofile'><a> Hola " + session.getAttribute("username")
								+ " bienvenido al panel de administraci�n. </a></div> ");

					}
					%>




			<div class="row" id="rowprod">
				<%
					List<Usuario> usuarios = (List<Usuario>) (session.getAttribute("listaUsuarios"));


					for (Usuario us : usuarios) {
						out.print("<form action='loginServlet' method='post'>");
						out.print("<div class='col-md-4 col-xs-4 col-sm-4'id='producthome'>");
						out.print("<p class='tituProducthome'> Nombre: " + us.getNombre() + "</p>");
						out.print("<p class='descProduct'> Apellidos: " + us.getApellido1()+" "+us.getApellido2()+"</p>");
						out.print("<p class='precioProduct'> Direcci�n: " + us.getCalle() +" "+us.getCiudad()+" "+us.getCodigopostal()+"</p>");
						out.print("<p class='categoriaProduct'>" + us.getPuerta()+" "+us.getPiso()+"</p>");
						out.print("<input type='submit' name='submit' type='submit' id='registproductbutton' value='Mostrar Usuario' class='button button-block'>");
						out.print("<input type='hidden' name='emailid' value=" + us.getEmail() + ">");
						out.print("<input type='hidden' value='mostrarUser' name='MostrarUser'>");
						out.print("</div>");
						out.print("</form>");
					}
				%>
			</div>
	



	</div>

</body>
</html>