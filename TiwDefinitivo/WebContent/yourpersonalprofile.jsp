
<%@ page import="java.util.List"%>
<%@ page import="model.Producto"%>
<%@ page import="model.Usuario"%>
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>T� perfil de usuario</title>

<link rel="stylesheet" href="css/styles.css" />
<link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<%
	if (session.getAttribute("username") == null) {

		response.sendRedirect("home.jsp");

	}
%>


<body id="cuerpo">

<nav class="navbar navbar-inverse" id="nav">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="logedin.jsp">WALLATIW</a>
		</div>
		<ul class="nav navbar-nav" id="lista">
			<li class="active"><a href="logedin.jsp">INICIO</a></li>
			<li><a href="profile.jsp">TUS PRODUCTOS</a></li>
			<li><a href="addproduct.jsp">A�ADIR PRODUCTOS</a></li>
			<li><a href="yourpersonalprofile.jsp">TU PERFIL</a></li>
			<%
			
			if(session.getAttribute("rol").equals("ADMIN")){
			out.print("<li><a href='userlist.jsp'>ADMIN</a></li>");
			}
			
			%>
			
			<li class="loginnavbar">
				<%
					if (session.getAttribute("username") != "null") {
						out.print("<a href='loginServlet?param=logout'> Hola " + session.getAttribute("username")
								+ "(Cerrar sesi�n) </a> ");

					}
				%>

			</li>
		</ul>
	</div>
	</nav>

		<div class="productos">



			<div class="row" id="rowprodview">
				<%
					//Producto prod = new Producto();
					//prod = (Producto) (request.getAttribute("producto"));
					
					Usuario user = new Usuario();
					user = (Usuario) (session.getAttribute("usuario"));

					if (user != null) {
						out.print("<div class='col-md-12 col-xs-12 col-sm-12 col-lg-12' id='productver'>");
						out.print("<p class='tituProducthome'> Nombre: " + user.getNombre() + "<p/>");
						out.print("<p class='descProduct'> Apellidos: " + user.getApellido1()+" "+user.getApellido2()+"<p/>");
						out.print("<p class='precioProduct'> Direcci�n: Calle" + user.getCalle() +" Ciudad: "+user.getCiudad()+" C�digo postal: "+user.getCodigopostal()+"<p/>");
						out.print("<p class='categoriaProduct'> Piso y puerta " + user.getPuerta()+" "+user.getPiso()+"<p/>");
						out.print("</div>");
					}

				%>
			</div>
		</div>
		
		<div id="cleaner">
		
		<%
		
		
		
		if(session.getAttribute("rol").equals("ADMIN")|session.getAttribute("email").equals(user.getEmail().toString())){
			
			
			out.print("<div id='productadm'>");
			out.print("<form id='formproduct2' action='loginServlet' method='post' enctype='multipart/form-data'>");
			
			out.print("<div class='insertproduct'>");
			out.print("<h1 id='prodedit'>EDICION DEL USUARIO</h1>");
			out.print("<input type='text' id='textadmin' placeholder='Nombre usuario' name='nombre'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin'placeholder='Apellido 1' name='apellido1'>");
			out.print("<br>");		
			out.print("<input type='text' id='textadmin' placeholder='Apellido 2' name='apellido2'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin' placeholder='Calle' name='calle'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin' placeholder='Ciudad' name='ciudad'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin' placeholder='C�digo postal' name='codigopostal'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin' placeholder='Piso' name='piso'>");
			out.print("<br>");	
			out.print("<input type='text' id='textadmin' placeholder='Puerta' name='puerta'>");
			out.print("<div id='editDiv'>");
			out.print("<input id='registproductbutton' type='submit' name='submit' value='Editar'>");
			out.print("</div>");
			out.print("<input type='hidden' name='rol' value='"+user.getRol()+"'>");
			out.print("<input type='hidden' name='emailid' value="+user.getEmail()+">");
			out.print("<input type='hidden' value='editarUsuario' name='editarusuario'>");
			out.print("</div>");
			out.print("</form>");
			out.print("<form id='formproduct2' action='loginServlet' method='post' enctype='multipart/form-data'>");
			out.print("<input id='registproductbutton' type='submit' name='submit' value='Borrar'>");
			out.print("<input type='hidden' name='userid' value="+user.getEmail()+">");
			out.print("<input type='hidden' value='borrarUsuario' name='borrarusuario'>");
			out.print("</form>");
			out.print("</div>");
			
		}
		
		
		%>



	</div>

	</div>

</body>
</html>