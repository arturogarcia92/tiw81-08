<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  
      <link rel="stylesheet" href="css/styleshome.css">

  
</head>





<body>
  <div class="form" >
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        
        <div id="signup">   
          <h1>Reg�strate gratis</h1>
          
            <%
            
           	 session.invalidate();  
             String message3 ="";
             String message2 ="";
			 String message = (String) request.getAttribute("registerStatus");
  			if(message!=null){
			out.println("<div class='fail'> <h3>  " +message+ " </h3>  </div>");
  			}
  			
  			
		 	message2 = (String) request.getAttribute("loginStatus");
			if(message2!=null){
				message="";
			out.println("<div class='fail'> <h3>  " +message2+ " </h3>  </div>");
				}
  			
			message3 = (String) request.getAttribute("logoutStatus");
			if(message3!=null){
				message="";
				message2="";
				out.println("<div class='fail'> <h3>  " +message3+ " </h3>  </div>");
				
			}
  			
  			
  			%>
          
          
          
          <form action="loginServlet" method="post">
          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                Nombre<span class="req">*</span>
              </label>
              <input id="username" name="nombre" placeholder=""
					title="Nombre" type="text" value="" size="40" required/>
            </div>
            
            <div class="field-wrap">
              <label>
                Primer apellido<span class="req">*</span>
              </label>
         <input id="primerapellido" name="primerapellido" placeholder=""
					title="PrimerApellido" type="text" value="" size="25"required/>
            </div>
            <div class="field-wrap">
              <label>
                Segundo apellido<span class="req">*</span>
              </label>
			<input id="segundoapellido" name="segundoapellido" placeholder=""
					title="SegundoApellido" type="text" value="" size="25"/>
            </div>

        
            <div class="field-wrap">
              <label>
                Contrase�a<span class="req">*</span>
              </label>
              <input id="password" name="contrase�a" placeholder=""
					title="Password" type="password" value="" size="25"required/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Direcci�n de e-mail<span class="req">*</span>
            </label>
            <input id="email" name="email" placeholder=""
					title="eMail" type="email" value="" size="30"required/>
          </div>
          
          <div class="field-wrap">
            <label>
              Ciudad<span class="req">*</span>
            </label>
            <input id="ciudad" name="ciudad" placeholder=""
					title="Ciudad" type="text" value="" size="25"required/>
          </div>
          
          <div class="field-wrap">
            <label>
              C�digo postal<span class="req">*</span>
            </label>
           <input id="codpostal" name="codpostal" placeholder=""
					title="codPostal" type="text" value="" size="5"required/>
          </div>
          
          <input type="submit" name="submit" type="submit" value="Register" class="button button-block"/>
          <input type="hidden" value="register" name="registrar"/>
          </form>

        </div>
        
        
        <!-- HACER QUE SE QUEDE COMO ACTIVA UNA PESTA�A U OTRA DEL FORM, HACIENDO QUE LA LISTA SE GENERE POR JSP-->
        
        <div id="login">   
          <h1>Entra en tu cuenta </h1>
          	
          	<% 

  			%>
          
          <form id="login2" action="loginServlet" method="post">
          
            <div class="field-wrap">
            <label>
              Introduce tu correo electr�nico<span class="req">*</span>
            </label>
            <input type="Email" name="username" required autocomplete="on"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Introduce tu contrase�a<span class="req">*</span>
            </label>
            <input type="password" name="password" required autocomplete="on"/>
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <input type="submit" name="submit" id="registproductbutton" value="Login"></input>
          
       <input type="hidden" value="login" name="logear"/>
          

          </form>
	
        </div>
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

      <script src="js/index.js"></script>

</body>
</html>
