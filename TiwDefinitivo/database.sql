-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: localhost    Database: wallatiw
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `email` varchar(50) NOT NULL,
  `productoid` int(10) NOT NULL AUTO_INCREMENT,
  `nombrecategoria` varchar(50) DEFAULT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(288) NOT NULL,
  `imagen` varchar(288) DEFAULT NULL,
  `precio` float NOT NULL,
  `visitas` int(10) DEFAULT NULL,
  `puntos` int(10) DEFAULT NULL,
  `informacion_estado` enum('VENTA','RESERVADO','VENDIDO') NOT NULL,
  PRIMARY KEY (`productoid`),
  KEY `fk_email` (`email`),
  KEY `fk_nombrecategoria_id` (`nombrecategoria`),
  CONSTRAINT `fk_email` FOREIGN KEY (`email`) REFERENCES `usuario` (`email`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES ('arturo@gmail.com',24,'Deportes','Balon real madrid','Firmado por C.Ronaldo','/TiwDefinitivo/images/1988063573.jpg',78,0,0,'VENTA'),('arturo@gmail.com',25,'Hogar','N�rdico del real madrid','Es de plumon','/TiwDefinitivo/images/2016692724.jpg',55,0,0,'VENTA'),('irene@gmail.com',27,'consolas','Play 3','RESERVADO','/TiwDefinitivo/images/1844917818.jpg',0,0,0,'VENTA');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `nombre` varchar(50) NOT NULL,
  `contrase�a` varchar(50) NOT NULL,
  `apellido1` varchar(50) NOT NULL,
  `apellido2` varchar(50) DEFAULT NULL,
  `calle` varchar(50) DEFAULT NULL,
  `piso` varchar(50) DEFAULT NULL,
  `puerta` varchar(50) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `codigopostal` varchar(50) DEFAULT NULL,
  `rol` enum('USUARIO','ADMIN') NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES ('rolando','12345','whey','protein','bureba','32','6c','leganes','28915','ADMIN','arturo@gmail.com'),('francisco','12345','perez','ard',NULL,NULL,NULL,'madrid','12451','USUARIO','francisco@gmail.com'),('IRENE','12345','gordilla','pe�as','odonell','13','1c','madrid','28928','USUARIO','irene@gmail.com'),('juan','12345','pob','sand','amap','3','a','gri�on','27834','USUARIO','juan@gmail.com'),('luis','1234','el','tal arranz',NULL,NULL,NULL,'madrid','13','USUARIO','luis@gmail.com'),('MARIANO','12345','YA NO ES AMO','amo',NULL,NULL,NULL,'barcelona','15785','USUARIO','mariano@gmail'),('pedriiissss','12345','pedr','rass','123','123','123','123213','12312','USUARIO','pedro@gmail.com'),('pipito','12345','grillo','ruiz',NULL,NULL,NULL,'logro�o','12321','USUARIO','pepe@gmail.com'),('pepe','12345','dcomngi','cast',NULL,NULL,NULL,'papaaa','123213','USUARIO','popo@gmail.com'),('rodolfin','12345','aaaa','ARIAS',NULL,NULL,NULL,'valencia','31345','USUARIO','rodolfo@gmail.com'),('roli','12345','rolas','roles',NULL,NULL,NULL,'rolandia','12321','USUARIO','rolanda@gmail.com');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-13 20:44:33